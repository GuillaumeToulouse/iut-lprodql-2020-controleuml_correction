using System;

namespace Tests
{
    public enum ListeEtats
    {
        Ferme,
        Ouvert
    }


    abstract class Porte
    {
        //private  ListeEtats etat;
        protected readonly string identifiant;

        public Porte(string identifiant)
        {
          this.identifiant = identifiant;
        }

        public string Identifiant {get { return this.identifiant;}}

        //internal ListeEtats Etat {get {return  etat;}}


    }
}