using System;
using NUnit.Framework;


namespace Tests
{
    public class Tests
    {



        [SetUp]
        public void Setup()
        {   


        }

        [Test]
        public void Test_Ouvre_Porte()
        {   
            //ARRANGE
            var porte = new PorteFermee("porte1");

            //ACT
            var nouvelEtatPorte =   porte.Ouvrir();
            // l'astuce c'est que c'est le TYPE de la porte qui traduit son état
            // l'avantage c'est qu'on ne peut pas changer d'état sans que ce soit cohérent avec un type 
            // (c'est le compilateur qui va faire les vérifications)



            //ASSET
            Assert.That( nouvelEtatPorte , Is.TypeOf<PorteOuverte>() );
        }

          [Test]
        public void Test_Ferme_Porte_Ouverte()
        {   
            //ARRANGE
            var porte = new PorteOuverte("porte2");

            //ACT
            //var nouvelEtatPorte =   porte.Ouvrir();
            Porte nouvelEtatPorte =   porte.Fermer();
            

            //ASSERT
            Assert.That( nouvelEtatPorte , Is.TypeOf<PorteFermee>() );
            Assert.That( nouvelEtatPorte.Identifiant, Is.EqualTo("porte2") );
        }


    }
}