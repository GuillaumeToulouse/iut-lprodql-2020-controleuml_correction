using System;

namespace Tests
{
    internal class PorteOuverte : Porte
    {
        public PorteOuverte(string identifiant): base(identifiant)
        {
            
        }
        internal Porte Fermer()
        {
          return new PorteFermee(this.identifiant);
        }
    }
}