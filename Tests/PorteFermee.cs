using System;

namespace Tests
{
    internal class PorteFermee : Porte
    {
        public PorteFermee(string identifiant) : base (identifiant)
        {
        }

        internal Porte Ouvrir()
        {
            return new PorteOuverte(this.identifiant);
        }
    }
}